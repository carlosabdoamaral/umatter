package com.carlosamaral.umatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmatterApplication {

	public static void main(String[] args) {
		SpringApplication.run(UmatterApplication.class, args);
	}

}
