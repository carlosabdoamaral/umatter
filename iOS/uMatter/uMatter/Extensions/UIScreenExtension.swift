//
//  UIScreenExtension.swift
//  uMatter
//
//  Created by Carlos Amaral on 27/01/22.
//

import Foundation
import UIKit

extension UIScreen {
    static var maxWidth = UIScreen.main.bounds.width
    static var maxHeight = UIScreen.main.bounds.height
}
